import Head from 'next/head';
import React from 'react';
import { Container } from './styles/layout';

const name = 'Joanna Chądzyńska';
export const siteTitle = 'Kinga Arii Yoga';

export interface LayoutProps {
	home?: boolean;
}

const Layout: React.SFC<LayoutProps> = ({ children, home, ...restProps }) => {
	return (
		<Container home={home} {...restProps}>
			<Head>
				<link rel='icon' href='/favicon.ico' />
				<meta
					name='description'
					content='Learn how to build a personal website using Next.js'
				/>
				<meta
					property='og:image'
					content={`https://og-image.now.sh/${encodeURI(
						siteTitle
					)}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.vercel.com%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
				/>
				<meta name='og:title' content={siteTitle} />
				<meta name='twitter:card' content='summary_large_image' />
				<meta name='viewport' content='width=device-width, initial-scale=1.0' />
				<link
					rel='stylesheet'
					type='text/css'
					charSet='UTF-8'
					href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css'
				/>
				<link
					rel='stylesheet'
					type='text/css'
					href='https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css'
				/>
			</Head>
			{children}
			{/* {!home && (
				<div>
					<Link href='/'>
						<a>← Back to home</a>
					</Link>
				</div>
			)} */}
		</Container>
	);
};

export default Layout;
