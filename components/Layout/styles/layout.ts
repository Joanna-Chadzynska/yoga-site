import styled from 'styled-components/macro';

type LayoutProps = {
	home?: boolean;
};

export const Container = styled.div<LayoutProps>`
	display: flex;
	flex-direction: column;
	height: 100%;
`;
