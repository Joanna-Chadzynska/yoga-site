import styled from 'styled-components/macro';
import { ButtonProps } from '../types';

export const Group = styled.div`
	position: relative;
`;

export const Container = styled.div`
	min-height: 200px;
	color: #666666;
	font-size: 36px;
	&:hover {
		cursor: -webkit-grab;
		cursor: grab;
	}
	&.dragging {
		cursor: -webkit-grabbing;
		cursor: grabbing;
	}
`;

export const Inner = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	padding: 1em;
`;

export const ImageContainer = styled.div`
	/* position: absolute; */
	top: 0;
	bottom: 0;
	margin: auto;
	height: 140px;
	width: 140px;
	overflow: hidden;
	/* clip-path: circle(50%); */
	@media screen and (min-width: 800px) {
		position: absolute;
		top: 0;
		bottom: 0;
	}
`;

export const Image = styled.img`
	display: block;
	height: auto;
	max-width: 100%;
`;

export const Body = styled.div`
	position: relative;
	z-index: 600;
	margin: 0;
	font-style: italic;
	padding: 1em;
`;

export const Cite = styled.blockquote`
	position: relative;
	z-index: 600;
	padding: 0;
	margin: 0;
	font-size: 20px;
	font-style: italic;
	line-height: 1.4 !important;
	font-family: Calibri;
	p {
		position: relative;
		margin-bottom: 20px;

		&:first-child::before {
			content: '“';
			color: rgba(0, 0, 0, 0.24);
			font-size: 7.5em;
			font-weight: 700;
			opacity: 1;
			position: absolute;
			top: -0.4em;
			left: -0.2em;
			text-shadow: none;
			z-index: -10;
		}
	}

	@media screen and (min-width: 800px) {
		padding: 40px 0 40px 180px;
	}
`;

export const Author = styled.cite`
	display: block;
	font-size: 14px;
	span {
		font-size: 16px;
		font-style: normal;
		letter-spacing: 1px;
		text-transform: uppercase;
	}

	@media screen and (min-width: 800px) {
		padding: 0 0 0px 180px;
	}
`;

export const Footer = styled.div``;

export const Button = styled.button<ButtonProps>`
	&::before {
		display: block;
		content: '';
		background-image: url(${({ icon }) => icon && icon});
		width: 40px;
		height: 40px;
	}

	&:last-of-type {
		right: 0;
	}
`;
