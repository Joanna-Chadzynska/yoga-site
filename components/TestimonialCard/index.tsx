import React from 'react';
import {
	Author,
	Body,
	Button,
	Cite,
	Container,
	Footer,
	Group,
	Image,
	ImageContainer,
	Inner,
} from './styles/testimonial';
import {
	ButtonProps,
	ImageProps,
	TestimonialComposition,
	TestimonialProps,
} from './types';

const Testimonial: React.SFC<TestimonialProps> & TestimonialComposition = ({
	children,
	...restProps
}) => {
	return (
		<Container {...restProps}>
			<Inner>{children}</Inner>
		</Container>
	);
};

export const TestimonialGroup: React.FC = ({ children, ...restProps }) => (
	<Group {...restProps}>{children}</Group>
);

export const TestimonialImage: React.FC<ImageProps> = ({
	src,
	alt,
	...restProps
}) => (
	<ImageContainer>
		<Image src={src} alt={alt} {...restProps} />
	</ImageContainer>
);

export const TestimonialBody: React.FC = ({ children, ...restProps }) => (
	<Body {...restProps}>{children}</Body>
);

export const TestimonialAuthor: React.FC = ({ children, ...restProps }) => (
	<Author {...restProps}>
		<span>{children}</span>
	</Author>
);

export const TestimonialCite: React.FC = ({ children, ...restProps }) => (
	<Cite {...restProps}>
		<p>{children}</p>
	</Cite>
);

export const TestimonialFooter: React.FC = ({ children, ...restProps }) => (
	<Footer {...restProps}>{children}</Footer>
);

export const TestimonialButton: React.FC<ButtonProps> = ({
	children,
	onClick,
	label,
	actionType,
	className,
	icon,
	...restProps
}) => (
	<Button
		actionType={actionType}
		onClick={onClick}
		data-role='none'
		className={className}
		aria-label={label}
		icon={icon}
		{...restProps}>
		{children}
	</Button>
);

Testimonial.Author = TestimonialAuthor;
Testimonial.Body = TestimonialBody;
Testimonial.Button = TestimonialButton;
Testimonial.Cite = TestimonialCite;
Testimonial.Group = TestimonialGroup;
Testimonial.Image = TestimonialImage;

export default Testimonial;
