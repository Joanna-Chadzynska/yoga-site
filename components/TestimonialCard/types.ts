export interface ButtonProps {
	onClick?: () => void;
	label?: 'prev' | 'next';
	actionType?: 'prev' | 'next';
	className?: string;
	icon?: string;
}

export interface ImageProps {
	src?: string;
	alt?: string;
}

export interface TestimonialProps {}

export interface TestimonialComposition {
	Group: React.FC;
	Image: React.FC<ImageProps>;
	Body: React.FC;
	Author: React.FC;
	Cite: React.FC;
	Button: React.FC<ButtonProps>;
}
