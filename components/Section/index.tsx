import React from 'react';
import { Wrapper } from '..';
import {
	ChunkImage,
	Container,
	Group,
	Inner,
	IntroContainer,
	IntroGroup,
	IntroImage,
	IntroTitle,
	List,
	ListItem,
	SubTitle,
	Text,
	TextGroup,
	Title,
} from './styles/section';
import {
	GroupProps,
	ImageProps,
	SectionComposition,
	SectionProps,
} from './types';

const Section: React.SFC<SectionProps> & SectionComposition = ({
	children,
	bg,
	id,
	...restProps
}) => {
	return (
		<Container id={id} bg={bg} {...restProps}>
			{children}
		</Container>
	);
};

export const SectionInner: React.FC = ({ children, ...restProps }) => (
	<Wrapper>
		<Inner {...restProps}>{children}</Inner>
	</Wrapper>
);

export const SectionTitle: React.FC = ({ children, ...restProps }) => (
	<Title {...restProps}>{children}</Title>
);

export const SectionSubTitle: React.FC = ({ children, ...restProps }) => (
	<SubTitle {...restProps}>{children}</SubTitle>
);

export const SectionIntroTitle: React.FC = ({ children, ...restProps }) => (
	<IntroTitle {...restProps}>{children}</IntroTitle>
);

export const SectionText: React.FC = ({ children, ...restProps }) => (
	<Text {...restProps}>{children}</Text>
);

export const SectionGroup: React.FC<GroupProps> = ({
	children,
	direction,
	...restProps
}) => <Group {...restProps}>{children}</Group>;

export const SectionTextGroup: React.FC = ({ children, ...restProps }) => (
	<TextGroup {...restProps}>{children}</TextGroup>
);

export const SectionImage: React.FC<ImageProps> = ({
	children,
	src,
	alt,
	...restProps
}) => <ChunkImage width='auto' height='auto' src={src} alt='joga position' />;

export const SectionIntroImage: React.FC<ImageProps> = ({
	children,
	src,
	alt,
	...restProps
}) => (
	<IntroImage src={src} alt={alt} height='auto' width='auto' {...restProps} />
);

export const SectionIntroGroup: React.FC = ({ children, ...restProps }) => (
	<IntroGroup {...restProps}>{children}</IntroGroup>
);

export const SectionIntroContainer: React.FC = ({ children, ...restProps }) => (
	<IntroContainer {...restProps}>{children}</IntroContainer>
);

export const SectionList: React.FC = ({ children, ...restProps }) => (
	<List {...restProps}>{children}</List>
);

export const SectionListItem: React.FC = ({ children, ...restProps }) => (
	<ListItem {...restProps}>{children}</ListItem>
);

Section.Inner = SectionInner;
Section.IntroContainer = SectionIntroContainer;
Section.IntroTitle = SectionIntroTitle;
Section.IntroImage = SectionIntroImage;
Section.IntroGroup = SectionIntroGroup;
Section.List = SectionList;
Section.ListItem = SectionListItem;
Section.SubTitle = SectionSubTitle;
Section.Text = SectionText;
Section.Title = SectionTitle;
Section.Group = SectionGroup;
Section.Image = SectionImage;
Section.TextGroup = SectionTextGroup;

export default Section;
