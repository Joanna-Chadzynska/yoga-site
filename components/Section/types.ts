export interface SectionProps {
	bg?: boolean;
	id?: string;
}

export interface ImageProps {
	src: string;
	alt?: string;
}

export interface GroupProps {
	reverse?: boolean;
	direction?: 'row' | 'row-reverse' | 'column' | 'column-reverse';
}

export interface SectionComposition {
	Inner: React.FC;
	IntroContainer: React.FC;
	IntroTitle: React.FC;
	IntroGroup: React.FC;
	Title: React.FC;
	SubTitle: React.FC;
	Text: React.FC;
	Group: React.FC<GroupProps>;
	Image: React.FC<ImageProps>;
	IntroImage: React.FC<ImageProps>;
	TextGroup: React.FC;
	List: React.FC;
	ListItem: React.FC;
}
