import Image from 'next/image';
import styled from 'styled-components/macro';
import { GroupProps, SectionProps } from './../types';

export const Container = styled.section<SectionProps>`
	width: 100%;
	position: relative;
	padding: 2em 0;

	background-color: ${({ bg, theme }) => bg && theme.colors.greyLight};
	${({ bg }) =>
		bg &&
		`
        @media screen and (min-width: 800px) {
            margin-top: -7em;
            margin-bottom: 0;
            padding-bottom: 0;
        }
        

    `}

	@media screen and (min-width: 800px) {
		padding: 0 0 2em 0;
	}
`;

export const Inner = styled.div`
	width: 100%;
	position: relative;
	@media screen and (min-width: 800px) {
		margin-top: 2em;
	}
`;

export const Text = styled.p`
	font-size: 16px;
	letter-spacing: 0.3px;
	line-height: 1.8em;
	text-align: justify;
	margin-bottom: 2em;
	span {
		display: block;
		margin: 1em 0;

		&:first-of-type {
			margin-top: 0;
		}
	}
	@media screen and (min-width: 550px) {
		margin-bottom: 0;
	}
`;

export const TextGroup = styled.div`
	display: flex;
	flex-direction: column;
	margin-top: 1em;

	@media screen and (min-width: 550px) {
		margin-top: 0;
		margin: 0 0 0 3em;
	}
`;

export const Title = styled.h1`
	text-align: center;
	font-size: clamp(2em, 5vw, 2.25em);
	letter-spacing: 2px;
	z-index: -1;
	line-height: 1.8;
	margin: 0 0 1em 0;

	@media screen and (min-width: 550px) {
		margin: 1.5em 0;
	}
`;

export const SubTitle = styled.h2`
	text-align: center;
	margin: 1em 0;
	@media screen and (min-width: 550px) {
		margin-bottom: 2em;
		&:first-of-type {
			margin-top: 2em;
		}
	}
`;

export const ChunkImage = styled(Image)`
	margin-bottom: 1em;
	height: 100%;
`;

export const Group = styled.div<GroupProps>`
	width: 100%;
	display: flex;
	flex-direction: column;

	@media screen and (min-width: 550px) {
		flex-direction: ${({ direction }) => (direction ? direction : 'row')};
		justify-content: space-between;
		${ChunkImage}, ${Text} {
			max-width: 47%;
		}
	}
`;

export const IntroContainer = styled.div`
	width: 100%;

	&::before {
		content: '';
		position: absolute;
		width: 300px;
		height: 300px;
		border-radius: 50%;
		background: ${({ theme }) => theme.colors.rose};
		opacity: 0.8;
		top: -12%;
		left: -20%;
		z-index: -1;
	}

	@media screen and (min-width: 800px) {
		display: grid;
		grid-template-columns: 40% auto;
		gap: 1em;
	}
`;

export const IntroTitle = styled.h2`
	text-align: center;
	font-size: clamp(1.875em, 5vw, 2.05em);
	line-height: 1.8em;
	margin-bottom: 1.25em;

	@media screen and (min-width: 550px) {
		margin-top: 1em;
	}

	@media screen and (min-width: 800px) {
		text-align: left;
	}
`;

export const IntroImage = styled.img`
	border-radius: 5px;
	opacity: 1;
	transform: translate(0px, 0px);
	z-index: 10;

	@media screen and (min-width: 800px) {
		margin-left: 2em;
		margin-top: 1em;
	}
`;

export const IntroGroup = styled.div`
	display: grid;
	width: 100%;
	margin-bottom: 3em;
	@media screen and (min-width: 800px) {
		margin-bottom: 5em;
	}
`;

export const List = styled.ul`
	margin: 0.5em 0;
`;

export const ListItem = styled.li`
	cursor: pointer;
	list-style-position: inside;
	padding: 0.5em;
	transition: all 0.2s ease-in-out;
	font-size: clamp(1rem, 5vw, 1.2rem);
	&::before {
		content: '🧘‍♂️';
		display: inline-block;
		margin: 0 1em;
	}
	&:hover {
		color: mediumaquamarine;
	}
`;
