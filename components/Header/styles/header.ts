import { motion } from 'framer-motion';
import Link from 'next/link';
import { transparentize } from 'polished';
import styled from 'styled-components/macro';

type HeaderProps = {
	bg?: string;
};

type NavProps = {
	variants?: boolean;
};

export const Container = styled.header<HeaderProps>`
	/* background-image: url(${(props) => props.bg && props.bg});
	background-repeat: no-repeat;
	background-size: cover;
	background-position: center; */
	min-height: 60vh;
	display: flex;
	flex-direction: column;
	position: relative;
`;

export const Inner = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	width: 300px;

	@media screen and (min-width: 800px) {
		position: static;
		width: 100%;
	}
`;

export const Banner = styled.div`
	max-width: 60rem;
	width: 100%;
	padding: 0 1rem;
	margin: 0 auto;
	height: 100%;
	display: grid;
	grid-template-columns: repeat(3, 1fr);
	grid-template-rows: repeat(3, 1fr);
	z-index: 1;
`;

export const BannerTitle = styled(motion.h1)`
	grid-column: 1 / 4;
	grid-row: 2 / 4;
	justify-self: center;
	color: #ffffff;
	font-size: clamp(2em, 5vw, 4em);
	text-shadow: 2px 2px 2px #474747;

	@media screen and (min-width: 450px) {
		grid-column: 1 / 3;
		grid-row: 2 / 3;
		align-self: center;
	}
`;

export const BannerQuote = styled(motion.figcaption)`
	align-self: center;
	grid-column: 1 / 4;
	font-family: ${({ theme }) => theme.font.familyFixed};
	font-size: clamp(1.5em, 5vw, 2em);
	font-weight: bold;
	display: flex;
	flex-direction: column;
	text-shadow: 3px 2px 3px rgba(255, 255, 255, 0.4);

	@media screen and (min-width: 800px) {
		grid-column: 2 / 4;
		justify-self: flex-end;
	}
`;

export const BannerSubTitle = styled(motion.blockquote)`
	/* margin-right: 1em; */
	@media screen and (min-width: 800px) {
		margin-right: 1em;
	}
`;

export const BannerCiteAuthor = styled.figcaption`
	align-self: flex-end;
`;

export const Nav = styled(motion.nav)`
	background: ${({ theme }) => transparentize(0.8, theme.text)};
	color: #ffffff;
	z-index: 10;
`;

export const NavGroup = styled(motion.ul)`
	position: absolute;
	top: 100px;
	right: 0;
	width: 230px;
	color: #000;
	display: flex;
	flex-direction: column;

	@media screen and (min-width: 800px) {
		position: static;
		top: 0;
		padding: 0;
		width: auto;
		flex-direction: row;
		align-items: center;
		color: #ffffff;
		/* gap: 2em; */
	}
`;

export const NavItem = styled(motion.li)`
	transition: 0.1s ease-in-out;
	margin: 1.5em 0;
	z-index: 2;
	@media screen and (min-width: 800px) {
		opacity: 1 !important;
		transform: none !important;
		margin: 0 1.5em;

		&:last-of-type {
			margin-right: 0;
		}

		&:hover a::before {
			color: red !important;
			content: '';
			position: absolute;
			left: 40%;
			bottom: -2px;
			width: 110%;
			height: 8px;
			transform: skew(-12deg) translateX(-50%);
			background: ${({ theme }) => theme.colors.green};
			opacity: 0.7;
			z-index: 2;
			transition: 0.3s ease-in-out;
		}
	}
`;

export const NavItemLink = styled.a`
	text-transform: uppercase;
	font-weight: 600;
	letter-spacing: 2px;
	position: relative;

	&::before {
		content: '';
		position: absolute;
		left: 40%;
		bottom: -2px;
		width: 110%;
		height: 12px;
		transform: skew(-12deg) translateX(-50%);
		background: ${({ theme }) => theme.colors.roseLight};
		opacity: 0.5;
		z-index: -1;
	}
`;

export const Logo = styled(Link)`
	cursor: pointer;
`;

export const LogoImage = styled.img`
	cursor: pointer;
	width: 80px;
	height: 80px;
	padding: 0.5em 0;
	z-index: 10;
`;

export const MenuToggle = styled.button`
	outline: none;
	border: none;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	margin: 0;
	padding: 0;
	cursor: pointer;
	position: absolute;
	top: 18px;
	right: 10px;
	width: 50px;
	height: 50px;
	border-radius: 50%;
	margin: 0 auto;
	background: transparent;
	display: block;
	z-index: 2;

	svg {
		margin: 0;
		padding: 0;
	}

	@media screen and (min-width: 800px) {
		display: none;
	}
`;

export const NavToggleBackground = styled(motion.div)`
	position: absolute;
	top: 0;
	right: 0;
	/* bottom: 0; */
	height: 150%;
	width: 300px;
	background: #fff;
	display: block;
	z-index: 1;

	@media screen and (min-width: 800px) {
		display: none;
	}
`;
// export const ProfileImage = styled.(Image)`
// 	width: 6rem;
// 	height: 6rem;
// `;

// export const ProfileHomeImage = styled.img`
// 	width: 8rem;
// 	height: 8rem;
// `;

// export const BackHome = styled.div`
// 	margin: 3rem 0 0;
// `;
