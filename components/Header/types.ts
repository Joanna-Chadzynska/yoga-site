export interface HeaderProps {
	bg?: string;
	title?: string;
	subtitle?: string;
}

export interface LinkProps {
	href: string;
	label?: string;
}

export interface MenuToggleProps {
	toggleMenu: () => void;
}

export interface NavGroupProps {
	variants?: any;
}

export interface NavItemProps {
	variants?: any;
	whileHover?: any;
	whileTap: any;
}

export interface NavToggleBackgroundProps {
	variants?: any;
}

export interface NavProps {
	navRef?: any;
	initial?: boolean;
	animate?: any;
	custom?: any;
}

export interface BannerProps {
	variants?: any;
	title?: string;
	subtitle?: string;
	author?: string;
}

export interface HeaderComposition {
	Banner: React.FC<BannerProps>;
	Nav: React.FC<NavProps>;
	Logo: React.FC;
	NavGroup: React.FC<NavGroupProps>;
	NavItem: React.FC<NavItemProps>;
	NavItemLink: React.FC<LinkProps>;
	MenuToggle: React.FC<MenuToggleProps>;
	ToggleBackground: React.FC<NavToggleBackgroundProps>;
}
