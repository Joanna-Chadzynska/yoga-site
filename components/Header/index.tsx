import { Wrapper } from 'components';
import { motion } from 'framer-motion';
import Image from 'next/image';
import React from 'react';
import {
	Banner,
	BannerCiteAuthor,
	BannerQuote,
	BannerSubTitle,
	BannerTitle,
	Container,
	Inner,
	Logo,
	LogoImage,
	MenuToggle,
	Nav,
	NavGroup,
	NavItem,
	NavItemLink,
	NavToggleBackground,
} from './styles/header';
import {
	BannerProps,
	HeaderComposition,
	HeaderProps,
	LinkProps,
	MenuToggleProps,
	NavGroupProps,
	NavItemProps,
	NavProps,
	NavToggleBackgroundProps,
} from './types';

const Path = (props) => (
	<motion.path
		strokeWidth='3'
		fill='transparent'
		stroke='hsl(0,0%, 18%)'
		strokeLinecap='round'
		{...props}
	/>
);

const Header: React.SFC<HeaderProps> & HeaderComposition = ({
	children,
	bg,
	title,
	subtitle,
	...restProps
}) => {
	return (
		<Container bg={`/images/banner/${bg}.JPG`} {...restProps}>
			<Image
				src={`/images/banner/${bg}.JPG`}
				layout='fill'
				objectFit='cover'
				objectPosition='center'
				alt={`Kinga Arii ${bg}`}
			/>
			{children}
		</Container>
	);
};

export const HeaderNav: React.FC<NavProps> = ({
	children,
	navRef,
	initial,
	animate,
	custom,
	...restProps
}) => (
	<Nav
		ref={navRef}
		initial={initial}
		animate={animate}
		custom={custom}
		{...restProps}>
		<Wrapper>
			<Inner>{children}</Inner>
		</Wrapper>
	</Nav>
);

export const HeaderNavGroup: React.FC<NavGroupProps> = ({
	children,
	variants,
	...restProps
}) => (
	<NavGroup variants={variants} {...restProps}>
		{children}
	</NavGroup>
);

export const HeaderNavItem: React.FC<NavItemProps> = ({
	children,
	variants,
	whileTap,
	whileHover,
	...restProps
}) => (
	<NavItem
		variants={variants}
		whileTap={whileTap}
		whileHover={whileHover}
		{...restProps}>
		{children}
	</NavItem>
);

export const HeaderNavItemLink: React.FC<LinkProps> = ({
	href,
	children,
	label,
	...restProps
}) => (
	<NavItemLink href={href} aria-label={label} {...restProps}>
		{children}
	</NavItemLink>
);

export const HeaderNavToggleBackground: React.FC<NavToggleBackgroundProps> = ({
	variants,

	...restProps
}) => <NavToggleBackground variants={variants} {...restProps} />;

export const HeaderLogo: React.FC<LinkProps> = ({
	href,
	children,
	...restProps
}) => (
	<Logo href='/' aria-label='Logo and home page' {...restProps}>
		<LogoImage
			src='/images/logo/logo.svg'
			alt='Kinga Arii Yoga logo'
			width='80'
			height='80'
		/>
	</Logo>
);

export const HeaderMenuToggle: React.FC<MenuToggleProps> = ({
	children,
	toggleMenu,
	...restProps
}) => (
	<MenuToggle
		onClick={toggleMenu}
		aria-label='toggle mobile menu'
		{...restProps}>
		<svg width='23' height='23' viewBox='0 0 23 23'>
			<Path
				variants={{
					closed: { d: 'M 2 2.5 L 20 2.5' },
					open: { d: 'M 3 16.5 L 17 2.5' },
				}}
			/>
			<Path
				d='M 2 9.423 L 20 9.423'
				variants={{
					closed: { opacity: 1 },
					open: { opacity: 0 },
				}}
				transition={{ duration: 0.1 }}
			/>
			<Path
				variants={{
					closed: { d: 'M 2 16.346 L 20 16.346' },
					open: { d: 'M 3 2.5 L 17 16.346' },
				}}
			/>
		</svg>
	</MenuToggle>
);

export const HeaderBanner: React.FC<BannerProps> = ({
	title,
	subtitle,
	variants,
	author,
	...restProps
}) => (
	<Banner {...restProps}>
		<BannerTitle variants={variants}>{title}</BannerTitle>
		{subtitle && (
			<BannerQuote>
				<BannerSubTitle>{subtitle}</BannerSubTitle>
				<BannerCiteAuthor> &mdash; {author}</BannerCiteAuthor>
			</BannerQuote>
		)}
	</Banner>
);

Header.Banner = HeaderBanner;
Header.Nav = HeaderNav;
Header.Logo = HeaderLogo;
Header.NavGroup = HeaderNavGroup;
Header.NavItem = HeaderNavItem;
Header.NavItemLink = HeaderNavItemLink;
Header.MenuToggle = HeaderMenuToggle;
Header.ToggleBackground = HeaderNavToggleBackground;

export default Header;
