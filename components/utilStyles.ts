import styled from 'styled-components/macro';

export const Heading2Xl = styled.h1`
	font-size: 2.5rem;
	line-height: 1.2;
	font-weight: 800;
	letter-spacing: -0.05rem;
	margin: 1rem 0;
`;

export const HeadingXl = styled.h1`
	font-size: 2rem;
	line-height: 1.3;
	font-weight: 800;
	letter-spacing: -0.05rem;
	margin: 1rem 0;
`;

export const HeadingLg = styled.h1`
	font-size: 1.5rem;
	line-height: 1.4;
	margin: 1rem 0;
`;

export const HeadingMd = styled.h1`
	font-size: 1.2rem;
	line-height: 1.5;
`;

export const BorderCircle = styled.img`
	border-radius: 9999px;
`;

export const ColorInherit = styled.a`
	color: inherit;
`;
