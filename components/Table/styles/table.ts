import styled from 'styled-components/macro';

export const Container = styled.div`
	/* max-width: 600px;
	margin: 0 auto; */
`;
// #1d96b2 - turkus
export const ColHead = styled.th`
	background-color: #047690;
	border: 1px solid #1d96b2;
	font-weight: normal;
	text-align: center;
	color: white;
`;

export const ColBody = styled.td``;

export const Head = styled.thead`
	position: absolute;
	clip: rect(1px 1px 1px 1px);
	/* IE6, IE7 */
	clip: rect(1px, 1px, 1px, 1px);
	padding: 0;
	border: 0;
	height: 1px;
	width: 1px;
	overflow: hidden;
	@media screen and (min-width: 600px) {
		position: relative;
		clip: auto;
		height: auto;
		width: auto;
		overflow: auto;
	}
`;

export const Body = styled.tbody`
	width: 100%;
`;

export const Row = styled.tr`
	color: #222;
	margin-bottom: 1em;
	border: 2px solid #1d96b2;
	line-height: 1.8;
`;

export const Inner = styled.table`
	width: 100%;
	margin-bottom: 0.5em;

	${Body}, ${ColHead}, ${ColBody} {
		display: block;
		padding: 0;
		white-space: normal;
	}

	${Row} {
		padding: 0;
		white-space: normal;
		display: grid;
		grid-template-rows: 1fr auto;
		grid-template-columns: repeat(2, 1fr);
		grid-template-areas:
			'header header'
			'hour spot';
	}

	${ColHead}[scope='row'] {
		grid-area: header;
	}

	${ColHead}, ${ColBody} {
		padding: 0.5em;
		vertical-align: middle;
	}

	@media screen and (min-width: 600px) {
		${Row} {
			display: table-row;
		}

		${ColHead}, ${ColBody} {
			display: table-cell;
			text-align: left;
		}

		${Body} {
			display: table-row-group;

			${ColHead}, ${ColBody} {
				border-bottom: 1px solid #1d96b2;
			}
		}

		${ColHead}[scope='row'] {
			background-color: transparent;
			color: #5e5d52;
			text-align: left;
			border: none;
		}
	}
`;

export const Caption = styled.caption``;

export const RowGroup = styled(ColBody)`
	display: flex;
`;

export const TextLink = styled.a`
	color: #047690;
	font-weight: 600;
`;
