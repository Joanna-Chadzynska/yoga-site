import React from 'react';
import {
	Body,
	Caption,
	ColBody,
	ColHead,
	Container,
	Head,
	Inner,
	Row,
	RowGroup,
	TextLink,
} from './styles/table';
import { ColProps, LinkProps, TableComposition, TableProps } from './types';

const Table: React.SFC<TableProps> & TableComposition = ({
	children,
	...restProps
}) => {
	return (
		<Container {...restProps}>
			<Inner>{children}</Inner>
		</Container>
	);
};

export const TableCaption: React.FC = ({ children, ...restProps }) => (
	<Caption {...restProps}>{children}</Caption>
);

export const TableHead: React.FC = ({ children, ...restProps }) => (
	<Head {...restProps}>{children}</Head>
);

export const TableBody: React.FC = ({ children, ...restProps }) => (
	<Body {...restProps}>{children}</Body>
);

export const TableRow: React.FC = ({ children, ...restProps }) => (
	<Row {...restProps}>{children}</Row>
);

export const TableRowGroup: React.FC = ({ children, ...restProps }) => (
	<RowGroup {...restProps}>{children}</RowGroup>
);

export const TableColHead: React.FC<ColProps> = ({
	scope,
	children,
	...restProps
}) => (
	<ColHead scope={scope} {...restProps}>
		{children}
	</ColHead>
);

export const TableColBody: React.FC<ColProps> = ({
	children,
	title,
	...restProps
}) => (
	<ColBody data-title={title} {...restProps}>
		{children}
	</ColBody>
);

export const TableTextLink: React.FC<LinkProps> = ({
	children,
	href,
	...restProps
}) => (
	<TextLink
		href={href}
		target='_blank'
		rel='noopener noreferrer'
		{...restProps}>
		{children}
	</TextLink>
);

Table.Body = TableBody;
Table.Caption = TableCaption;
Table.ColBody = TableColBody;
Table.ColHead = TableColHead;
Table.Head = TableHead;
Table.Row = TableRow;
Table.RowGroup = TableRowGroup;
Table.TextLink = TableTextLink;

export default Table;
