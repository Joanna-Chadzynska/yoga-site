export interface TableProps {}

export interface LinkProps {
	href: string;
}

export interface ColBodyProps {
	title?: string;
}

export interface ColProps {
	title?: string;
	scope?: string;
}

export interface TableComposition {
	Body: React.FC;
	Caption: React.FC;
	ColBody: React.FC<ColProps>;
	ColHead: React.FC<ColProps>;
	Head: React.FC;
	Row: React.FC;
	RowGroup: React.FC;
	TextLink: React.FC<LinkProps>;
}
