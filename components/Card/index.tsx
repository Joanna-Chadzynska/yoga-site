import Link from 'next/link';
import React from 'react';
import { Background, Container, Group, Title } from './styles/card';
import { CardComposition, CardProps } from './types';

const Card: React.SFC<CardProps> & CardComposition = ({
	children,
	bg,
	id,
	label,
	...restProps
}) => {
	return (
		<Container {...restProps}>
			<Link href={`/yogaStyles/${id}`}>
				<Background aria-label={label} bg={`/images/yogaStyles/${bg}.JPG`}>
					{children}
				</Background>
			</Link>
		</Container>
	);
};

export const CardGroup: React.FC = ({ children, ...restProps }) => (
	<Group {...restProps}>{children}</Group>
);

export const CardTitle: React.FC = ({ children, ...restProps }) => (
	<Title {...restProps}>{children}</Title>
);

Card.Group = CardGroup;
Card.Title = CardTitle;

export default Card;
