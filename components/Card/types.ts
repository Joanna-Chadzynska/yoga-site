export interface CardProps {
	bg?: string;
	id?: string;
	label?: string;
}

export interface CardComposition {
	Group: React.FC;
	Title: React.FC;
}
