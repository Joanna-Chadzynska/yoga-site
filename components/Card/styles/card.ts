// import { motion } from 'framer-motion';
// import { transparentize } from 'polished';
import styled from 'styled-components/macro';
import { CardProps } from '../types';

export const Background = styled.div<CardProps>`
	background-image: url(${({ bg }) => bg && bg});
	background-repeat: no-repeat;
	background-position: center;
	background-size: cover;
	width: 100%;
	height: 100%;
	transition: all 0.5s;
	display: flex;
	justify-content: center;
	align-items: center;

	&::before {
		content: '';
		display: none;
		height: 100%;
		width: 100%;
		position: absolute;
		top: 0;
		left: 0;
		background-color: rgba(52, 73, 94, 0.75);
	}
`;

export const Title = styled.p`
	display: none;
	color: #ffffff;
	text-transform: uppercase;
	font-weight: bold;
	font-size: clamp(1.75rem, 5vw, 2rem);
	margin: auto;
	transition: transform 0.3s ease-in;
`;

export const Container = styled.li<CardProps>`
	border: none;
	border-radius: 2em;
	box-shadow: 3px 2px 5px rgba(0, 0, 0, 0.5);
	cursor: pointer;
	margin-bottom: 1em;
	height: 500px;
	min-width: 300px;
	width: 100%;
	overflow: hidden;
	position: relative;

	@media screen and (min-width: 800px) {
		transform: translate(0px, 5.625rem);
	}

	&:hover ${Background}, &:focus ${Background} {
		transform: scale(1.1);
		filter: grayscale(50%);
	}

	&:hover ${Title}, &:focus ${Title} {
		display: block;
	}
`;

export const Group = styled.ul`
	display: grid;
	grid-gap: 2em;
	@media screen and (min-width: 800px) {
		grid-template-columns: repeat(3, auto);
	}
`;
