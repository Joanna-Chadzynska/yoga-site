import styled from 'styled-components/macro';

const Wrapper = styled.div`
	max-width: 60rem;
	width: 100%;
	padding: 0 1rem;
	margin: 0 auto;
	/* margin: 3rem auto 6rem; */
`;

export default Wrapper;
