import { lighten } from 'polished';
import styled from 'styled-components/macro';

export const Container = styled.footer`
	flex-shrink: 0;
	background-color: ${({ theme }) => lighten(0.05, theme.colors.green)};
	color: #ffffff;

	svg {
		width: 40px;
		height: 40px;
	}
`;

export const Inner = styled.div`
	padding: 2em 0;
`;

export const Socials = styled.div`
	display: flex;
	justify-content: center;
	align-items: center;
`;

export const IconLink = styled.a`
	transition: transform 0.3s ease-in-out;
	&:hover {
		transform: scale(1.5);
	}
	margin: 0 1rem;
	font-size: 2.5rem;
	@media screen and (min-width: 800px) {
		margin: 0 1em;
		font-size: 2.25rem;
	}
`;
