import React from 'react';
import { Wrapper } from '..';
import { Container, IconLink, Inner, Socials } from './styles/footer';

export interface FooterProps {
	id?: string;
}
export interface FooterComposition {
	Socials: React.FC;
	LinkIcon: React.FC<LinkProps>;
}

export interface LinkProps {
	href?: string;
	label?: string;
}

const Footer: React.SFC<FooterProps> & FooterComposition = ({
	children,
	id,
	...restProps
}) => {
	return (
		<Container id={id} {...restProps}>
			<Wrapper>
				<Inner>{children}</Inner>
			</Wrapper>
		</Container>
	);
};

export const FooterSocials: React.FC = ({ children, ...restProps }) => (
	<Socials {...restProps}>{children}</Socials>
);

export const FooterIconLink: React.FC<LinkProps> = ({
	children,
	href,
	label,
	...restProps
}) => (
	<IconLink
		href={href}
		aria-label={label}
		target='_blank'
		rel='noopener noreferrer'
		{...restProps}>
		{children}
	</IconLink>
);

Footer.Socials = FooterSocials;
Footer.LinkIcon = FooterIconLink;

export default Footer;
