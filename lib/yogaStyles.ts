import fs from 'fs';
import matter from 'gray-matter';
import renderToString from 'next-mdx-remote/render-to-string';
import path from 'path';

// POSTS_PATH is useful when you want to get the path to a specific file
export const YOGA_STYLES_PATH = path.join(process.cwd(), 'yogaStyles');

// postFilePaths is the list of all mdx files inside the POSTS_PATH directory
export const yogaStyleFilePaths = fs
	.readdirSync(YOGA_STYLES_PATH)
	// Only include md(x) files
	.filter((path) => /\.mdx?$/.test(path));

export function getAllYogaStylesData() {
	const yogaStylesData = yogaStyleFilePaths.map((filePath) => {
		const id = filePath.replace(/\.mdx?$/, '');
		const source = fs.readFileSync(path.join(YOGA_STYLES_PATH, filePath));
		const { content, data } = matter(source);

		return {
			content,
			data,
			filePath,
			id,
		};
	});

	return yogaStylesData;
}

export function getAllYogaStylesIds() {
	const paths = yogaStyleFilePaths
		// Remove file extensions for page paths
		.map((path) => path.replace(/\.mdx?$/, ''))
		// Map the path into the static paths object required by Next.js
		.map((id) => ({ params: { id } }));
	return paths;
}

export async function getYogaStylesData(
	id: string | string[],
	components?: any
) {
	const postFilePath = path.join(YOGA_STYLES_PATH, `${id}.mdx`);
	const source = fs.readFileSync(postFilePath);

	const { content, data } = matter(source);

	const mdxSource = await renderToString(content, {
		components,
		// Optionally pass remark/rehype plugins
		mdxOptions: {
			remarkPlugins: [],
			rehypePlugins: [],
		},
		scope: data,
	});

	// const hydratedContent = hydrate(mdxSource, { components });

	return {
		data,
		content,
		mdxSource,
	};
}
