import { Section } from 'components';
import React from 'react';

export interface YogaStylesHomeProps {}

const HomeIntro: React.SFC<YogaStylesHomeProps> = () => {
	return (
		<Section>
			<Section.Inner>
				<Section.IntroContainer>
					<Section.IntroGroup>
						<Section.IntroTitle>
							Styl jogi dopasowany do Ciebie
						</Section.IntroTitle>
						<Section.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolor
							dignissimos rerum possimus ea laboriosam quaerat veniam explicabo
							incidunt praesentium voluptatibus nemo quo exercitationem
						</Section.Text>
					</Section.IntroGroup>

					<Section.IntroImage src='/images/about/tooth.JPG' alt='yoga pose' />
				</Section.IntroContainer>
			</Section.Inner>
		</Section>
	);
};

export default HomeIntro;
