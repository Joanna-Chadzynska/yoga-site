import { Table } from 'components';
import schedules from 'data/schedule.json';
import React from 'react';
export interface ScheduleContainerProps {}

const ScheduleContainer: React.SFC<ScheduleContainerProps> = () => {
	return (
		<Table>
			<Table.Head>
				<Table.Row>
					<Table.ColHead scope='col'>Dzień</Table.ColHead>
					<Table.ColHead scope='col'>Godziny</Table.ColHead>
					<Table.ColHead scope='col'>Miejsce</Table.ColHead>
				</Table.Row>
			</Table.Head>

			<Table.Body>
				{schedules.map((schedule) => {
					if (!schedule.spot) return;
					return (
						<Table.Row key={schedule.id}>
							<Table.ColHead scope='row'>{schedule.day}</Table.ColHead>
							<Table.ColBody title='Godziny'>
								{schedule.hours.map((hour) => (
									<span key={hour}>
										<span key={hour}>{hour}</span>
										<br />
									</span>
								))}
							</Table.ColBody>
							<Table.ColBody>
								<Table.TextLink href={schedule.spotUrl}>
									{schedule.spot}
								</Table.TextLink>
								<br />
								{schedule.address}
							</Table.ColBody>
						</Table.Row>
					);
				})}
			</Table.Body>
		</Table>
	);
};

export default ScheduleContainer;
