import { Header, Layout } from 'components';
import React from 'react';
import { Footer, Navigation } from '..';

export interface LayoutContainerProps {
	home?: boolean;
	bg?: string;
	title?: string;
	subtitle?: string;
	author?: string;
}

const LayoutContainer: React.SFC<LayoutContainerProps> = ({
	children,
	home,
	bg,
	title,
	subtitle,
	author,
}) => {
	return (
		<Layout home={home}>
			<Header bg={bg}>
				<Navigation />
				<Header.Banner author={author} title={title} subtitle={subtitle} />
			</Header>

			<main>{children}</main>

			<Footer />
		</Layout>
	);
};

export default LayoutContainer;
