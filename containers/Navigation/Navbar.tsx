import { Header } from 'components';
import React from 'react';

export interface NavbarProps {}

const variants = {
	open: {
		transition: { staggerChildren: 0.07, delayChildren: 0.2 },
	},
	closed: {
		transition: { staggerChildren: 0.05, staggerDirection: -1 },
	},
};

const variantsItem = {
	open: {
		y: 0,
		opacity: 1,
		transition: {
			y: { stiffness: 1000, velocity: -100 },
		},
	},
	closed: {
		y: 50,
		opacity: 0,
		transition: {
			y: { stiffness: 1000 },
		},
	},
};

const Navbar: React.SFC<NavbarProps> = () => {
	return (
		<Header.NavGroup variants={variants}>
			<Header.NavItem
				variants={variantsItem}
				whileHover={{ scale: 1.1 }}
				whileTap={{ scale: 0.95 }}>
				<Header.NavItemLink href='/#about' label='about link'>
					O mnie
				</Header.NavItemLink>
			</Header.NavItem>
			<Header.NavItem
				variants={variantsItem}
				whileHover={{ scale: 1.1 }}
				whileTap={{ scale: 0.95 }}>
				<Header.NavItemLink href='/yogaStyles' label='yoga styles page link'>
					Joga
				</Header.NavItemLink>
			</Header.NavItem>
			<Header.NavItem
				variants={variantsItem}
				whileHover={{ scale: 1.1 }}
				whileTap={{ scale: 0.95 }}>
				<Header.NavItemLink href='/schedule' label='schedule page link'>
					Grafik
				</Header.NavItemLink>
			</Header.NavItem>
			<Header.NavItem
				variants={variantsItem}
				whileHover={{ scale: 1.1 }}
				whileTap={{ scale: 0.95 }}>
				<Header.NavItemLink href='#contact' label='contact link'>
					Kontakt
				</Header.NavItemLink>
			</Header.NavItem>
			<Header.NavItem
				variants={variantsItem}
				whileHover={{ scale: 1.1 }}
				whileTap={{ scale: 0.95 }}>
				<Header.NavItemLink href='/testimonials' label='testimonials page link'>
					Rekomendacje
				</Header.NavItemLink>
			</Header.NavItem>
		</Header.NavGroup>
	);
};

export default Navbar;
