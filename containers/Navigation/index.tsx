import { Header } from 'components';
import { useCycle } from 'framer-motion';
import { useDimensions } from 'hooks';
import React, { useRef } from 'react';
import Navbar from './Navbar';

export interface NavigationProps {}

const sidebar = {
	open: (height = 1000) => ({
		clipPath: `circle(${height * 2 + 200}px at 251px 40px)`,
		transition: {
			type: 'spring',
			stiffness: 20,
			restDelta: 2,
		},
	}),
	closed: {
		clipPath: 'circle(30px at 251px 40px)',
		transition: {
			delay: 0.5,
			type: 'spring',
			stiffness: 400,
			damping: 40,
		},
	},
};

const Navigation: React.SFC<NavigationProps> = () => {
	const [isOpen, toggleOpen] = useCycle(false, true);
	const containerRef = useRef<HTMLHtmlElement>(null);
	const { height } = useDimensions(containerRef);
	return (
		<Header.Nav
			initial={false}
			animate={isOpen ? 'open' : 'closed'}
			custom={height}
			navRef={containerRef}>
			<Header.Logo />
			<Header.ToggleBackground variants={sidebar} />
			<Navbar />
			<Header.MenuToggle toggleMenu={() => toggleOpen()} />
		</Header.Nav>
	);
};

export default Navigation;
