import { Section } from 'components';
import React from 'react';

export interface AboutMeProps {}

const AboutMe: React.SFC<AboutMeProps> = () => {
	return (
		<Section id='about'>
			<Section.Inner>
				<Section.Title>O Mnie</Section.Title>
				<Section.Group>
					<Section.Image
						src='/images/about/about.JPG'
						alt='Kinga Arii profile photo'
					/>
					<Section.Text>
						<span>
							Lorem ipsum, dolor sit amet consectetur adipisicing elit. Delectus
							numquam labore provident laudantium fugit. Ut praesentium sapiente
							eius excepturi a necessitatibus iusto accusamus facilis eveniet
							distinctio exercitationem, tempore architecto qui repudiandae
							laudantium harum beatae accusantium cum sint quam obcaecati vel,
							ipsum nihil. Nihil dolor veritatis iure delectus, mollitia sint
							quod.
						</span>

						<span>
							Lorem ipsum dolor, sit amet consectetur adipisicing elit. Cum et
							possimus at asperiores ea eum maxime similique? Provident
							assumenda corrupti repudiandae praesentium ad qui, laborum minima
							omnis voluptatibus est deleniti eveniet repellendus quae! Laborum,
							deserunt. Earum iure, blanditiis incidunt tempora dolores
							dignissimos, magnam fugit itaque at sunt, corrupti tempore
							pariatur!
						</span>
						<span>
							Lorem, ipsum dolor sit amet consectetur adipisicing elit.
							Necessitatibus dolorem dolor, quaerat tempore corporis iure ullam
							animi quisquam obcaecati itaque magnam? Nemo repellendus eos
							quidem vero, animi odio molestiae amet.
						</span>
					</Section.Text>
				</Section.Group>
			</Section.Inner>
		</Section>
	);
};

export default AboutMe;
