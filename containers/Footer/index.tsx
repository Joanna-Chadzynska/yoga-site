import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Footer } from 'components';
import React from 'react';

export interface FooterContainerProps {}

const FooterContainer: React.SFC<FooterContainerProps> = () => {
	return (
		<Footer id='contact'>
			<Footer.Socials>
				<Footer.LinkIcon
					href='https://www.instagram.com/kingaariiyoga/'
					label='Kinga Arii Yoga instagram'>
					<FontAwesomeIcon icon={['fab', 'instagram']} />
				</Footer.LinkIcon>

				<Footer.LinkIcon
					href='https://www.facebook.com/kingaariiyoga'
					label='Kinga Arii Yoga facebook'>
					<FontAwesomeIcon icon={['fab', 'facebook']} />
				</Footer.LinkIcon>

				<Footer.LinkIcon
					href='mailto:kingaarii@wp.pl;jmalek87@gmail.com?subject=Zapytanie o kurs'
					label='Kinga Arii Yoga email'>
					<FontAwesomeIcon icon={['fas', 'envelope']} />
				</Footer.LinkIcon>

				<Footer.LinkIcon href='tel:+48731666097' label='Kinga Arii Yoga phone'>
					<FontAwesomeIcon icon={['fas', 'phone-alt']} />
				</Footer.LinkIcon>
			</Footer.Socials>
		</Footer>
	);
};

export default FooterContainer;
