import { Card, Section } from 'components';
import React from 'react';

export interface HomeYogaStylesProps {}

const HomeYogaStyles: React.SFC<HomeYogaStylesProps> = () => {
	return (
		<Section bg>
			<Section.Inner>
				<Card.Group>
					<Card bg='flow' id='flow' label='flow'>
						<Card.Title>Flow</Card.Title>
					</Card>
					<Card bg='restorative' id='restorative' label='restorative'>
						<Card.Title>Restorative</Card.Title>
					</Card>
					<Card bg='mindful' id='mindful' label='mindful'>
						<Card.Title>Mindful</Card.Title>
					</Card>
				</Card.Group>
			</Section.Inner>
		</Section>
	);
};

export default HomeYogaStyles;
