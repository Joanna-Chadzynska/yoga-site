import 'styled-components';

declare module 'styled-components' {
	export interface DefaultTheme {
		body: string;
		text: string;
		colors: {
			greyLight: string;
			greyDark: string;
			greyCadet: string;
			rose: string;
			roseLight: string;
			green: string;
		};
		font: {
			family: string;
			familyFixed: string;
			weight: string;
			weightBold: string;
			weightExtrabold: string;
		};
		size: {
			elementHeight: string;
			elementMargin: string;
			letterSpacing: string;
			letterSpacingAlt: string;
		};
		duration: { transitions: string; menu: string; fadeIn: string };
		misc: {
			maxSpotlights: string;
			maxFeatures: string;
			zIndexBase: string;
		};
	}
}
