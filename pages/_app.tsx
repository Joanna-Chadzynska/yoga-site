import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { AnimatePresence, motion } from 'framer-motion';
import { AppProps } from 'next/app';
import { FC } from 'react';
import { ThemeProvider } from 'styled-components';
import { GlobalStyles } from 'styles/GlobalStyles';
import '../public/fonts/amatic.css';
import '../public/fonts/poppins.css';
import theme from '../styles/theme';

library.add(fab, fas);

interface Props {
	routeKey?: any;
	slide?: number;
	slideUp?: number;
	key2?: any;
}

const MountTransition: FC<Props> = ({
	slide = 0,
	slideUp = 0,
	children,
	routeKey,
}) => (
	<motion.div
		key={routeKey}
		exit={{ opacity: 0, x: slide, y: slideUp }}
		initial={{
			opacity: 0,
			x: slide,
			y: slideUp,
		}}
		animate={{
			opacity: 1,
			x: 0,
			y: 0,
		}}
		transition={{
			ease: 'easeInOut',
			delay: 0.5,
			duration: 0.5,
		}}
		style={{
			height: '100%',
		}}>
		{children}
	</motion.div>
);

function MyApp({ Component, pageProps, router }: AppProps) {
	// The handler to smoothly scroll the element into view
	const handleExitComplete = (): void => {
		if (typeof window !== 'undefined') {
			// Get the hash from the url
			const hashId = window.location.hash;

			if (hashId) {
				// Use the hash to find the first element with that id
				const element = document.querySelector(hashId);

				if (element) {
					// Smooth scroll to that elment
					element.scrollIntoView({
						behavior: 'smooth',
						block: 'start',
						inline: 'nearest',
					});
				}
			}
		}
	};
	return (
		<ThemeProvider theme={theme}>
			<GlobalStyles />
			<AnimatePresence exitBeforeEnter onExitComplete={handleExitComplete}>
				<MountTransition routeKey={router.route} slide={30}>
					<Component {...pageProps} />
				</MountTransition>
			</AnimatePresence>
		</ThemeProvider>
	);
}

export default MyApp;
