import { Section } from 'components';
import { Layout } from 'containers';
import fs from 'fs';
import matter from 'gray-matter';
import { yogaStyleFilePaths, YOGA_STYLES_PATH } from 'lib/yogaStyles';
import { GetStaticProps } from 'next';
import Head from 'next/head';
import Link from 'next/link';
import path from 'path';
import React from 'react';

export default function YogaStyles({
	allYogaStylesData,
}: {
	allYogaStylesData: {
		data: {
			title: string;
			date: string;
		};
		filePath: string;
		id: string;
	}[];
}) {
	return (
		<Layout bg='styles' title='Style Jogi'>
			<Head>
				<title>Joga</title>
			</Head>
			<Section>
				<Section.Inner>
					<Section.SubTitle>Joga - czym jest i co nam daje?</Section.SubTitle>
					<Section.Group direction='row-reverse'>
						<Section.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora
							dolore est nesciunt et, nobis officiis quia! Sunt quis sequi
							minima inventore laudantium voluptatum perferendis vero dicta
							excepturi. Animi ex dignissimos aliquid explicabo omnis quaerat
							debitis iusto. Dolores ipsa vel cupiditate quod iusto vero
							expedita sint, accusamus voluptatem laudantium, id nemo.
							<span>
								Lorem ipsum dolor sit amet, consectetur adipisicing elit.
								Explicabo alias quam eveniet minima iure odio sed nostrum saepe
								corporis, natus repudiandae quia similique ad aperiam dolor!
								Quisquam quia saepe culpa.
							</span>
						</Section.Text>
						<Section.Image src='images/positions/pose1.jpg' />
					</Section.Group>

					<Section.SubTitle>Dla kogo joga?</Section.SubTitle>
					<Section.Group>
						<Section.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora
							dolore est nesciunt et, nobis officiis quia! Sunt quis sequi
							minima inventore laudantium voluptatum perferendis vero dicta
							excepturi. Animi ex dignissimos aliquid explicabo omnis quaerat
							debitis iusto. Dolores ipsa vel cupiditate quod iusto vero
							expedita sint, accusamus voluptatem laudantium, id nemo.
						</Section.Text>
						<Section.Image src='images/positions/woman_with_kid.jpg' />
					</Section.Group>

					<Section.SubTitle>Jak zacząć?</Section.SubTitle>
					<Section.Group direction='row-reverse'>
						<Section.Text>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora
							dolore est nesciunt et, nobis officiis quia! Sunt quis sequi
							minima inventore laudantium voluptatum perferendis vero dicta
							excepturi. Animi ex dignissimos aliquid explicabo omnis quaerat
							debitis iusto. Dolores ipsa vel cupiditate quod iusto vero
							expedita sint, accusamus voluptatem laudantium, id nemo.
						</Section.Text>
						<Section.Image src='images/positions/mat.jpg' />
					</Section.Group>

					<Section.SubTitle>Jaki styl jogi wybrać?</Section.SubTitle>
					<Section.Text>
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi,
						doloremque nisi. Est dicta, magnam accusamus maiores placeat laborum
						esse vero.
					</Section.Text>

					<Section.List>
						{allYogaStylesData &&
							allYogaStylesData.map((style) => (
								<Section.ListItem key={style.filePath}>
									<Link
										as={`/yogaStyles/${style.filePath.replace(/\.mdx?$/, '')}`}
										href={`/yogaStyles/[id]`}>
										<a>{style.data.title}</a>
									</Link>
								</Section.ListItem>
							))}
					</Section.List>
				</Section.Inner>
			</Section>
		</Layout>
	);
}

export const getStaticProps: GetStaticProps = async () => {
	const allYogaStylesData = yogaStyleFilePaths.map((filePath) => {
		const id = filePath.replace(/\.mdx?$/, '');
		const source = fs.readFileSync(path.join(YOGA_STYLES_PATH, filePath));
		const { content, data } = matter(source);

		return {
			content,
			data,
			filePath,
			id,
		};
	});
	return {
		props: {
			allYogaStylesData,
		},
	};
};
