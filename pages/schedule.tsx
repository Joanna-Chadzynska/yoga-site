import { Section } from 'components';
import { Layout, ScheduleContainer } from 'containers';
import Head from 'next/head';
import React from 'react';

export default function Schedule({
	allPostsData,
}: {
	allPostsData: {
		date: string;
		title: string;
		id: string;
	}[];
}) {
	return (
		<Layout bg='schedule' title='Grafik'>
			<Head>
				<title>Grafik</title>
			</Head>
			<Section>
				<Section.Inner>
					<Section.Title>Grafik zajęć</Section.Title>
					<Section.Text>
						Zajęcia yogi prowadzone są w przyjaznej i życzliwej atmosferze.
						Kameralność moich grup sprawia że dostosowuje asany indywidualnie do
						ciebie. Zapraszam jeśli jesteś osobą początkującą jak i
						zaawansowaną, na pewno znajdziesz coś wyjątkowego dla siebie.
					</Section.Text>
					<br />

					<ScheduleContainer />
					<br />
					<Section.Text>
						Zachęcam też do zajęć indywidualnych, które mogą wspierać cię w
						okresie przeciążenia, zmęczenia i braku energii. Polecam szczególnie
						w momentach kiedy potrzebujesz skupić się na realizacji swoich
						marzeń i celów, zwiększyć poziom energii oraz poprawić swoje
						samopoczucie.
					</Section.Text>
				</Section.Inner>
			</Section>
		</Layout>
	);
}
