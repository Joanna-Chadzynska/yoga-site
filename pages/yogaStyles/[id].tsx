import { Section } from 'components';
import { Layout } from 'containers';
import { getAllYogaStylesIds, getYogaStylesData } from 'lib/yogaStyles';
import { GetStaticPaths, GetStaticProps } from 'next';
import hydrate from 'next-mdx-remote/hydrate';
import Head from 'next/head';
import Link from 'next/link';
import React from 'react';

const components = {
	p: Section.Text,
	ul: Section.List,
	li: Section.ListItem,
};

export default function YogaStyle({
	data,
	content,
	mdxSource,
}: {
	data: {
		title: string;
		id: string;
		date: string;
	};
	content: string;
	mdxSource: {
		compiledSource: string;
		renderedOutput: string;
		scope: {
			title: string;
			date: string;
			id: string;
			subtitle: string;
			author: string;
		};
	};
}) {
	const hydratedContent = hydrate(mdxSource, { components });

	return (
		<Layout bg={data.id} title={data.title}>
			<Head>
				<title>{data.title}</title>
			</Head>
			<Section>
				<Section.Inner>
					{hydratedContent}

					<Link href='/yogaStyles'>
						<a>👈 Powrót do listy zajęć </a>
					</Link>
				</Section.Inner>
			</Section>
		</Layout>
	);
}

export const getStaticPaths: GetStaticPaths = async () => {
	const paths = getAllYogaStylesIds();
	return {
		paths,
		fallback: false,
	};
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
	const data = await getYogaStylesData(params.id, components);

	return {
		props: {
			content: data.content,
			data: data.data,
			mdxSource: data.mdxSource,
		},
	};
};
