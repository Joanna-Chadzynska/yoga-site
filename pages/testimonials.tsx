import { Section, Testimonial } from 'components';
import { Layout } from 'containers';
import recommendations from 'data/recommendations.json';
import Head from 'next/head';
import React from 'react';
import Slider from 'react-slick';

const PrevArrow = (props) => {
	const { className, style, onClick } = props;
	return (
		<Testimonial.Button
			icon='icons/arrowPrev.svg'
			className={className}
			onClick={onClick}
		/>
	);
};
const NextArrow = (props) => {
	const { className, style, onClick } = props;
	return (
		<Testimonial.Button
			icon='icons/arrowNext.svg'
			className={className}
			onClick={onClick}
		/>
	);
};

const CustomSlide = (props) => {
	const { index, children } = props;
	return (
		<Testimonial {...props}>
			<>{index}</>
		</Testimonial>
	);
};

const Testimonials = () => {
	const settings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1,
		swipeToSlide: true,
		nextArrow: <NextArrow />,
		prevArrow: <PrevArrow />,
		// autoplay: true,
		autoplaySpeed: 5000,
		pauseOnHover: true,
		adaptiveHeight: true,
	};
	return (
		<Layout bg='testimonials' title='Rekomendacje'>
			<Head>
				<title>Rekomendacje</title>
			</Head>

			<Section>
				<Section.Inner>
					<Section.Text>
						"Zrób światu prezent w postaci siebie samej - zrelaksowanej,
						serdecznej i w pełni obecnej!" Judith H. Lasater
					</Section.Text>
					<Section.Text>
						"Zrób światu prezent w postaci siebie samej - zrelaksowanej,
						serdecznej i w pełni obecnej!" Judith H. Lasater
					</Section.Text>
					<Section.Text>
						"Zrób światu prezent w postaci siebie samej - zrelaksowanej,
						serdecznej i w pełni obecnej!" Judith H. Lasater
					</Section.Text>
					<Testimonial.Group>
						<Slider {...settings}>
							{recommendations.map((rec) => (
								<Testimonial key={rec.id}>
									<Testimonial.Image
										src={rec.image}
										alt={`photo of ${rec.author}`}
									/>
									<Testimonial.Body>
										<Testimonial.Cite>{rec.description}</Testimonial.Cite>
										<Testimonial.Author>{rec.author}</Testimonial.Author>
									</Testimonial.Body>
								</Testimonial>
							))}
						</Slider>
					</Testimonial.Group>
				</Section.Inner>
			</Section>
		</Layout>
	);
};

export default Testimonials;
