import { siteTitle } from 'components/Layout';
import { AboutMe, HomeIntro, HomeYogaStyles, Layout } from 'containers';
import { GetStaticProps } from 'next';
import Head from 'next/head';
import React from 'react';
import { getSortedPostsData } from '../lib/posts';

export default function Home({
	allPostsData,
}: {
	allPostsData: {
		date: string;
		title: string;
		id: string;
	}[];
}) {
	return (
		<Layout
			home
			bg='home'
			title='Kinga Arii Yoga'
			subtitle='Nawet najdalsza podróż zaczyna się od pierwszego kroku'
			author='Lao Tzu'>
			<Head>
				<title>{siteTitle}</title>
			</Head>
			<br />
			<HomeIntro />
			<br />
			<HomeYogaStyles />
			<br />
			<AboutMe />
			<br />
		</Layout>
	);
}

export const getStaticProps: GetStaticProps = async () => {
	const allPostsData = getSortedPostsData();
	return {
		props: {
			allPostsData,
		},
	};
};
