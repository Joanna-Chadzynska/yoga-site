import { Layout } from 'containers';
import Head from 'next/head';
import * as React from 'react';

export default function Custom404() {
	return (
		<Layout home bg='home'>
			<Head>
				<title>Nie znaleziono strony</title>
			</Head>
			<h1>404 - Page Not Found</h1>;
		</Layout>
	);
}
