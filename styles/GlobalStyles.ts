import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
    *,
    *::after,
    *::before {
        box-sizing: border-box;
    }

    /* Remove default padding */
    ul,
    ol,
    body {
        padding: 0;
    }

    /* Remove default margin */
    body,
    h1,
    h2,
    h3,
    h4,
    p,
    ul,
    ol,
    figure,
    blockquote,
    dl,
    dd {
        margin: 0;
    }

    html,
    body {
        height: 100%;
    }

    html,
    body {
        color: ${({ theme }) => theme.text};
    	padding: 0;
    	margin: 0;
    	font-family: ${({ theme }) => theme.font.family};
        line-height: 1.5;
        scroll-behavior: smooth;
        text-rendering: optimizeSpeed;
    }

    #__next {
        display: flex;
        flex-direction: column;
        height: 100%;
    }

    main {
		flex: 1 0 auto;
		/* margin-top: 4rem; */
		/* padding: 0 1em; */
	}
    
    a {
    	color: inherit;
        text-decoration: none;
        cursor: pointer
    }

    button {
        cursor: pointer
    }

    ul,
    ol {
        list-style: none;
    }
    
    /* Make images easier to work with */
    img,
    svg,
    video,
    canvas,
    audio,
    iframe,
    embed,
    object {
        max-width: 100%;
        display: block;
    }

    /* Natural flow and rhythm in articles by default */
    article > * + * {
        margin-top: 1em;
    }

    /* Inherit fonts for inputs and buttons */
    input,
    button,
    textarea,
    select {
 font: inherit
    }   

    img {
        max-width: 100%;
    }
`;
