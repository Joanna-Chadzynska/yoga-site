import { DefaultTheme } from 'styled-components';

const theme: DefaultTheme = {
	body: '#ffffff',
	text: '#1e1e24',
	colors: {
		greyLight: '#E0E3E6',
		greyDark: '#899EBD',
		greyCadet: '#8EA3BF',
		rose: '#B88D96',
		roseLight: '#BF95A0',
		green: '#04917C',
	},
	font: {
		family: "'Poppins', Helvetica, sans-serif",
		familyFixed: "'Amatic SC','Courier New', cursive",
		weight: '400',
		weightBold: '600',
		weightExtrabold: '800',
	},
	size: {
		elementHeight: '2.75em',
		elementMargin: '2em',
		letterSpacing: '0.075em',
		letterSpacingAlt: '0.225em',
	},
	duration: { transitions: '0.2s', menu: '0.5s', fadeIn: '3s' },
	misc: {
		maxSpotlights: '10',
		maxFeatures: '10',
		zIndexBase: '10000',
	},
};

export default theme;
