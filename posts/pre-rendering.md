---
title: 'Two Forms of Pre-rendering'
date: '2020-01-01'
---

<p>Next.js has two forms of pre-rendering: **Static Generation** and **Server-side
Rendering**. The difference is in **when** it generates the HTML for a page.</p>

<ul>
    <li><strong>Static Generation</strong> is the pre-rendering method that generates the HTML at
        <em>build time</em>. The pre-rendered HTML is then _reused_ on each request.</li>
    <li><strong>Server-side Rendering</strong> is the pre-rendering method that generates the HTML
  on <em>each request</em></li>
</ul>

<p>Importantly, Next.js lets you **choose** which pre-rendering form to use for
each page. You can create a "hybrid" Next.js app by using Static Generation for
most pages and using Server-side Rendering for others.</p>
